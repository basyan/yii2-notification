<?php

use basyan\notification\DevinoSmsNotifier;
use Faker\Generator;

class DevinoSmsNotifierTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    /** @var Generator */
    protected $faker;
    /** @var DevinoSmsNotifier */
    protected $notifier;
    /** @var bool */
    protected $enableTest = true;

    protected function _before()
    {
        $this->notifier = Yii::$app->devinoSms;
        $this->faker = Faker\Factory::create();
        $this->enableTest = empty($_ENV['DEVINO_TEST_OFF']);
    }

    protected function _after()
    {
    }


    // tests
    public function testSendMessage()
    {
        if ($this->enableTest) {
            $this->notifier->message->text = $this->faker->text;
            $this->tester->assertTrue($this->notifier->message->send());
        }
    }

    // tests
    public function testHasErrors()
    {
        if ($this->enableTest) {
            $this->notifier->password = uniqid();
            $message = new \basyan\notification\Message($this->notifier);
            $message->text = $this->faker->text;
            $message->send();
            $this->tester->assertTrue($message->hasErrors());
        }
    }
}