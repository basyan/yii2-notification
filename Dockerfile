FROM yiisoftware/yii2-php:7.2-fpm
RUN docker-php-ext-enable xdebug
RUN apt-get update \
    && apt-get install -y \
    git \
    openssh-client
