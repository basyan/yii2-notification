<?php

namespace basyan\notification\log;

use basyan\notification\TelegramNotifier;
use yii\base\InvalidConfigException;
use yii\log\Target;

/**
 * Class TelegramTarget
 * @package basyan\notification\log
 * @property TelegramNotifier $notifier
 * @property-write string $chatId
 * @property-write string $token
 */
class TelegramTarget extends Target
{
    private $chatId;
    private $token;
    private $notifier;

    /**
     * @param string $chatId
     */
    public function setChatId(string $chatId): void
    {
        $this->chatId = $chatId;
    }

    /**
     * @param string $token
     */
    public function setToken(string $token): void
    {
        $this->token = $token;
    }

    public function init()
    {
        if (!$this->chatId) {
            throw new InvalidConfigException('The "chatId" option must be set.');
        }
        if (!$this->token) {
            throw new InvalidConfigException('The "token" option must be set.');
        }
        $this->notifier = new TelegramNotifier();
        $this->notifier->chatId = $this->chatId;
        $this->notifier->token = $this->token;

        parent::init();
    }

    public function export()
    {
        $this->notifier->message->text = implode("\n", array_map([$this, 'formatMessage'], $this->messages));
        $this->notifier->message->send();
    }
}
