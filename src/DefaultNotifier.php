<?php

namespace basyan\notification;

use yii\base\Component;

/**
 * Class DefaultNotifier
 * @package basyan\notification
 * @property Message $message
 */
abstract class DefaultNotifier extends Component implements NotifierInterface
{
    private $message;

    public function init()
    {
        $this->message = new Message($this);
        parent::init();
    }

    /**
     * @return Message
     */
    public function getMessage(): Message
    {
        return $this->message;
    }
}
