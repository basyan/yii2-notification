<?php

namespace basyan\notification;

use TelegramBot\Api\BotApi;
use TelegramBot\Api\Exception;
use TelegramBot\Api\InvalidArgumentException;

/**
 * Class TelegramNotifier
 * @package basyan\notification
 * @property-write string $chatId
 * @property-write string $token
 */
class TelegramNotifier extends DefaultNotifier
{
    private $chatId;
    private $token;

    /**
     * @param string $chatId
     */
    public function setChatId(string $chatId): void
    {
        $this->chatId = $chatId;
    }

    /**
     * @param string $token
     */
    public function setToken(string $token): void
    {
        $this->token = $token;
    }

    public function sendMessage(Message $message): bool
    {
        try {
            $bot = new BotApi($this->token);
            if ($bot->sendMessage($this->chatId, mb_substr($message->text, 0, 4096))) {
                return true;
            }
        } catch (InvalidArgumentException $e) {
            $message->addErrors([$e->getMessage()]);
        } catch (Exception $e) {
            $message->addErrors([$e->getMessage()]);
        }
        return false;
    }
}
