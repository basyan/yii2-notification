<?php

use basyan\notification\BitrixNotifier;
use basyan\notification\DevinoSmsNotifier;
use basyan\notification\TelegramNotifier;

/**
 * Class Yii
 * Used for enhanced IDE code autocompletion.
 */
class Yii extends \yii\BaseYii
{
    /** @var App */
    public static $app;
}

/**
 * Class App
 * @property TelegramNotifier $telegram
 * @property DevinoSmsNotifier $devinoSms
 * @property BitrixNotifier $bitrix
 */
class App extends \yii\web\Application
{
}
